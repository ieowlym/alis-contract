import "./App.css";
import { Contracts } from "./components/Contracts";

function App() {
  return (
    <div className="App">
      <Contracts />
    </div>
  );
}

export default App;
