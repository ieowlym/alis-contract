import styled from "@emotion/styled";
import "@fontsource/montserrat";

const ContractCard = styled("div")`
  /* card */

  font-family: "Montserrat";
  box-sizing: border-box;

  /* Auto layout */

  display: flex;
  flex-direction: column;
  padding: 0px;

  width: 316px;
  height: 228px;

  /* Neutral/1 */

  background: #ffffff;
  /* Neutral/4 */

  border: 1px solid #f0f0f0;
  border-radius: 10px;
`;

const TitleWrapper = styled("div")`
  /* title-wrapper */

  box-sizing: border-box;
  width: 316px;

  /* Auto layout */

  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 16px 24px;
  gap: 10px;

  height: 56px;

  /* Neutral/1 */

  /* border & divider/divider ↓ */

  box-shadow: inset 0px -1px 0px #f0f0f0;

  /* Inside auto layout */

  flex: none;
  order: 0;
  align-self: stretch;
  flex-grow: 0;

  > h5 {
    margin: 0;

    /* text */

    width: 144px;
    height: 24px;

    /* h5 */

    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;

    /* black */

    color: #293452;

    /* Inside auto layout */

    flex: none;
    order: 0;
    flex-grow: 0;
  }
  > div {
    /* Right */

    width: 16px;
    height: 16px;

    /* Inside auto layout */

    flex: none;
    order: 1;
    flex-grow: 0;
  }
`;

const BodyWrapper = styled("div")`
  /* body */

  box-sizing: border-box;

  /* Auto layout */

  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 24px;
  gap: 8px;

  width: 316px;
  height: 132px;

  /* Neutral/1 */

  /* Inside auto layout */

  flex: none;
  order: 1;
  align-self: stretch;
  flex-grow: 0;

  > h5 {
    margin: 0;

    /* text */

    width: 217px;
    height: 24px;

    /* h5 */

    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    /* dark-grey */

    color: rgba(0, 40, 119, 0.45);

    /* Inside auto layout */

    flex: none;
    order: 0;
    flex-grow: 0;
  }
`;

const StatusTag = styled("div")`
  .default {
    box-sizing: border-box;

    /* Auto layout */

    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 1px 8px;
    gap: 8px;

    height: 22px;

    /* Neutral/2 */

    background: #fafafa;
    /* silver */

    border: 1px solid rgba(41, 52, 82, 0.25);
    border-radius: 2px;

    /* Inside auto layout */

    flex: none;
    order: 1;
    flex-grow: 0;

    > p {
      /* waiting */

      width: 105px;
      height: 20px;

      /* footnote */

      font-family: "Montserrat";
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 20px;
      /* identical to box height, or 167% */

      /* black */

      color: #293452;

      /* Inside auto layout */

      flex: none;
      order: 1;
      flex-grow: 0;
    }
  }
`;

const Message = styled("div")`
  /* message */

  /* Auto layout */

  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;
  gap: 8px;

  min-width: 30px;
  height: 22px;

  /* Inside auto layout */

  flex: none;
  order: 2;
  flex-grow: 0;
  > * {
    display: flex;
  }
`;

const ActionsWrapper = styled("div")`
  /* actions */

  border-radius: 0 0 10px 10px;

  /* Auto layout */

  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px;

  width: 316px;
  height: 40px;

  /* Neutral/1 */

  /* border & divider/divider ↑ */

  box-shadow: inset 0px 1px 0px #f0f0f0;

  /* Inside auto layout */

  flex: none;
  order: 2;
  align-self: stretch;
  flex-grow: 0;

  > button {
    background: transparent;
  }
`;

const ExtendButtonWtapper = styled("button")`
  font-family: "Montserrat";
  color: rgba(41, 52, 82, 0.25);
  /* icon-wrapper-16px */

  border: none;

  /* Auto layout */

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0px;

  width: 158px;
  height: 24px;

  /* Neutral/1 */

  /* border & divider/divider → */
  box-shadow: inset -1px 0px 0px #f0f0f0;

  /* Inside auto layout */

  flex: none;
  order: 0;
  flex-grow: 1;
`;

const DownLoadButtonWrapper = styled("button")`
  font-family: "Montserrat";
  color: #dc1832;
  /* icon-wrapper-16px */

  border: none;

  border-radius: 0 0 10px 0;

  /* Auto layout */

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0px;
  gap: 8px;

  width: 159px;
  height: 39px;

  /* Neutral/1 */

  /* border & divider/divider → */

  /* Inside auto layout */

  flex: none;
  order: 1;
  flex-grow: 1;
`;

const TitleArrow = () => {
  return (
    <svg
      width="9"
      height="14"
      viewBox="0 0 9 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8.53036 6.55042L0.480359 0.262918C0.459324 0.246358 0.434044 0.236066 0.407424 0.233226C0.380803 0.230385 0.353922 0.23511 0.329866 0.246858C0.30581 0.258606 0.285555 0.276901 0.271428 0.299642C0.257301 0.322383 0.249875 0.348647 0.250002 0.375418V1.75578C0.250002 1.84328 0.291073 1.9272 0.35893 1.98078L6.7875 7.00042L0.35893 12.0201C0.289287 12.0736 0.250002 12.1576 0.250002 12.2451V13.6254C0.250002 13.7451 0.387502 13.8111 0.480359 13.7379L8.53036 7.45042C8.59878 7.39705 8.65413 7.32878 8.6922 7.2508C8.73027 7.17282 8.75005 7.08719 8.75005 7.00042C8.75005 6.91364 8.73027 6.82801 8.6922 6.75004C8.65413 6.67206 8.59878 6.60379 8.53036 6.55042Z"
        fill="#DC1832"
      />
    </svg>
  );
};

const TagClock = () => {
  return (
    <svg
      width="11"
      height="12"
      viewBox="0 0 11 12"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      style={{
        width: "12px",
        height: "12px",
      }}
    >
      <path
        d="M5.25 0.75C2.35078 0.75 0 3.10078 0 6C0 8.89922 2.35078 11.25 5.25 11.25C8.14922 11.25 10.5 8.89922 10.5 6C10.5 3.10078 8.14922 0.75 5.25 0.75ZM5.25 10.3594C2.84297 10.3594 0.890625 8.40703 0.890625 6C0.890625 3.59297 2.84297 1.64062 5.25 1.64062C7.65703 1.64062 9.60938 3.59297 9.60938 6C9.60938 8.40703 7.65703 10.3594 5.25 10.3594Z"
        fill="#293452"
      />
      <path
        d="M7.29736 7.4825L5.62626 6.27429V3.3739C5.62626 3.32234 5.58408 3.28015 5.53251 3.28015H4.96884C4.91728 3.28015 4.87509 3.32234 4.87509 3.3739V6.60125C4.87509 6.63171 4.88915 6.65984 4.91376 6.67742L6.85204 8.0907C6.89423 8.12117 6.95283 8.11179 6.98329 8.07078L7.31845 7.61375C7.34892 7.57039 7.33954 7.51179 7.29736 7.4825Z"
        fill="#293452"
      />
    </svg>
  );
};

const RedCircle = () => {
  return (
    <svg
      width="6"
      height="6"
      viewBox="0 0 6 6"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="3" cy="3" r="3" fill="#DC1832" />
    </svg>
  );
};

const MessageIcon = () => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_1_34061)">
        <path
          d="M7.14296 8C7.14296 8.22733 7.23326 8.44535 7.39401 8.60609C7.55475 8.76684 7.77277 8.85714 8.0001 8.85714C8.22743 8.85714 8.44545 8.76684 8.60619 8.60609C8.76694 8.44535 8.85724 8.22733 8.85724 8C8.85724 7.77267 8.76694 7.55465 8.60619 7.39391C8.44545 7.23316 8.22743 7.14286 8.0001 7.14286C7.77277 7.14286 7.55475 7.23316 7.39401 7.39391C7.23326 7.55465 7.14296 7.77267 7.14296 8ZM10.7144 8C10.7144 8.22733 10.8047 8.44535 10.9654 8.60609C11.1262 8.76684 11.3442 8.85714 11.5715 8.85714C11.7989 8.85714 12.0169 8.76684 12.1776 8.60609C12.3384 8.44535 12.4287 8.22733 12.4287 8C12.4287 7.77267 12.3384 7.55465 12.1776 7.39391C12.0169 7.23316 11.7989 7.14286 11.5715 7.14286C11.3442 7.14286 11.1262 7.23316 10.9654 7.39391C10.8047 7.55465 10.7144 7.77267 10.7144 8ZM3.57153 8C3.57153 8.22733 3.66184 8.44535 3.82258 8.60609C3.98333 8.76684 4.20134 8.85714 4.42867 8.85714C4.656 8.85714 4.87402 8.76684 5.03476 8.60609C5.19551 8.44535 5.28582 8.22733 5.28582 8C5.28582 7.77267 5.19551 7.55465 5.03476 7.39391C4.87402 7.23316 4.656 7.14286 4.42867 7.14286C4.20134 7.14286 3.98333 7.23316 3.82258 7.39391C3.66184 7.55465 3.57153 7.77267 3.57153 8ZM15.3787 4.9C14.9751 3.94107 14.3965 3.08036 13.659 2.34107C12.9267 1.60607 12.0573 1.02182 11.1001 0.621429C10.118 0.208929 9.0751 0 8.0001 0H7.96439C6.88224 0.00535714 5.83403 0.219643 4.84832 0.641071C3.89931 1.04557 3.03807 1.63085 2.3126 2.36429C1.58224 3.10179 1.00903 3.95893 0.612601 4.91429C0.201887 5.90357 -0.00525583 6.95536 0.000101308 8.0375C0.00616068 9.27763 0.299551 10.4995 0.857244 11.6071V14.3214C0.857244 14.5393 0.943787 14.7482 1.09783 14.9023C1.25188 15.0563 1.46082 15.1429 1.67867 15.1429H4.39474C5.50241 15.7006 6.72426 15.9939 7.96439 16H8.00189C9.07153 16 10.109 15.7929 11.0858 15.3875C12.0382 14.9919 12.9043 14.4144 13.6358 13.6875C14.3733 12.9571 14.9537 12.1036 15.359 11.1518C15.7805 10.1661 15.9947 9.11786 16.0001 8.03572C16.0055 6.94821 15.7947 5.89286 15.3787 4.9ZM12.6805 12.7214C11.4287 13.9607 9.76796 14.6429 8.0001 14.6429H7.96974C6.89296 14.6375 5.82332 14.3696 4.87867 13.8661L4.72867 13.7857H2.21439V11.2714L2.13403 11.1214C1.63046 10.1768 1.3626 9.10714 1.35724 8.03036C1.3501 6.25 2.03046 4.57857 3.27867 3.31964C4.5251 2.06071 6.19117 1.36429 7.97153 1.35714H8.00189C8.89474 1.35714 9.76081 1.53036 10.5769 1.87321C11.3733 2.20714 12.0876 2.6875 12.7019 3.30179C13.3144 3.91429 13.7965 4.63036 14.1305 5.42679C14.4769 6.25179 14.6501 7.12679 14.6465 8.03036C14.6358 9.80893 13.9376 11.475 12.6805 12.7214Z"
          fill="#002877"
          fill-opacity="0.45"
        />
      </g>
      <defs>
        <clipPath id="clip0_1_34061">
          <rect width="16" height="16" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

const DownloadIcon = () => {
  return (
    <svg
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M7.88736 10.6574C7.90072 10.6744 7.9178 10.6883 7.93729 10.6977C7.95678 10.7072 7.97818 10.7122 7.99986 10.7122C8.02154 10.7122 8.04294 10.7072 8.06243 10.6977C8.08192 10.6883 8.099 10.6744 8.11236 10.6574L10.1124 8.12701C10.1856 8.03415 10.1195 7.89665 9.99986 7.89665H8.67665V1.85379C8.67665 1.77522 8.61236 1.71094 8.53379 1.71094H7.46236C7.38379 1.71094 7.3195 1.77522 7.3195 1.85379V7.89487H5.99986C5.88022 7.89487 5.81415 8.03237 5.88736 8.12522L7.88736 10.6574ZM14.5356 10.0324H13.4641C13.3856 10.0324 13.3213 10.0967 13.3213 10.1752V12.9252H2.67843V10.1752C2.67843 10.0967 2.61415 10.0324 2.53557 10.0324H1.46415C1.38557 10.0324 1.32129 10.0967 1.32129 10.1752V13.7109C1.32129 14.027 1.57665 14.2824 1.89272 14.2824H14.107C14.4231 14.2824 14.6784 14.027 14.6784 13.7109V10.1752C14.6784 10.0967 14.6141 10.0324 14.5356 10.0324Z"
        fill="#DC1832"
      />
    </svg>
  );
};

export const Contract = () => {
  return (
    <ContractCard>
      <TitleWrapper>
        <h5>Договор № 12338</h5>
        <TitleArrow />
      </TitleWrapper>
      <BodyWrapper>
        <h5>4 июн 2022 - 12 июн 2022</h5>
        <StatusTag>
          <div className="default">
            <TagClock />
            <p>На утверждении</p>
          </div>
        </StatusTag>
        <Message>
          <span className="new-icon">
            <RedCircle />
          </span>

          <span className="message">
            <MessageIcon />
          </span>
          <span>2</span>
        </Message>
      </BodyWrapper>
      <ActionsWrapper>
        <ExtendButtonWtapper>Продлить</ExtendButtonWtapper>
        <DownLoadButtonWrapper>
          <DownloadIcon />
          Скачать
        </DownLoadButtonWrapper>
      </ActionsWrapper>
    </ContractCard>
  );
};
