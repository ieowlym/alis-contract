import styled from "@emotion/styled";
import { Contract } from "./Contract";

const FLexGrid = styled("div")`
  display: block;
  margin: auto;
  margin-top: 30px;
  max-width: 1313px;
  display: flex;
  flex-wrap: wrap;
  gap: 16px;
`;

export const Contracts = () => {
  return (
    <FLexGrid>
      <Contract />
      <Contract />
      <Contract />
      <Contract />
    </FLexGrid>
  );
};
